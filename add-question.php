<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();

/* Variables */
$text = $_GET['text'];
$fileURL = $_GET['fileURL'];
	
// Get current User 
$currUser = ParseUser::getCurrentUser();

// SAVE QUESTION ----------------
$qObj = new ParseObject("Questions");
$qObj->set('text', strip_tags($text));

// Make an array of keywords
$keywords = preg_split('/\s+/', strtolower($text));
$qObj->setArray('keywords', $keywords);

$qObj->set('userPointer', $currUser);
$qObj->set('isReported', false);
$qObj->set('answers', 0);
$qObj->set('likes', 0);
$qObj->setArray('likedBy', array());

// Save image (if any)
if ($fileURL != "" || $fileURL != null) {
	$file = ParseFile::createFromFile($fileURL, "image.jpg");
  	$file->save();
  	$qObj->set("image", $file);

 	$qObj->set("hasImage", true);
} else {
	$qObj->set("hasImage", false);
}


try {
	$qObj->save();
	
	echo 'QUESTION SAVED';

// error in query Ads
} catch (ParseException $e){ echo $e->getMessage(); }
?>