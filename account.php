<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();

// Go back to Login in case you're logged out
$currUser = ParseUser::getCurrentUser();
if ($currUser) { 
} else { header("Refresh:0; url=login.php"); }
?>

<!-- header -->
<?php include 'header.php' ?>

<body>
    <div class="container">

        <!-- title -->
        <div>
            <h2><img src="assets/img/80.png" width="28"> <a href="index.php">AskIt</a> <small>| A place for questions</small></h2>
        </div>
        <br><!-- ./ TITLE -->

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <!-- justified navbar -->
                <div class="navbar navbar-default navbar-justified">
                    <ul class="nav navbar-nav">
                        <!-- back button -->
                        <li>
                            <a href="javascript:history.go(-1)">
                            <em class="fa fa-arrow-left"></em> <span class="button-text"> Back</span></a>
                        </li>
                        <!-- logout button -->
                        <li>
                            <a href="#">
                            <em class="fa fa-sign-out"></em> <span class="button-text" onclick="logOut()"> Logout</span></a>
                        </li>
                    </ul>
                </div>
            </div><!-- ./ justified navbar -->

            <div class="row">
    		  <div class="col-sm-12 col-lg-offset-0 col-lg-12">

<?php					
	// Get fullName
    $fullName = $currUser->get('fullName');
    // Get email
    $email = $currUser->get('email');
	// Get avatar
    $file = $currUser->get('avatar');
   
    $defaultAvatar = "https://". $_SERVER['SERVER_NAME']."/askit/assets/img/default-user.png";
    $defaultBanner = "https://". $_SERVER['SERVER_NAME']."/askit/assets/img/default-banner.png";
    
    if ($file) {
        $avatarURL = $file->getURL();
    } else {
        $avatarURL = $defaultAvatar;
    }
    

    
?>
                
                <div class="page-header text-center"> 
            	   <h3>ACCOUNT</h3>
        	       <br>
                   <!-- avatar image -->
       		       <img class="img-circle center-cropped-avatar-80" src="<?php echo $avatarURL ?>">
                   <br>
                   <!-- full name -->
                   <h4><?php echo $fullName ?></h4>
                   <!-- activity button -->
                   <a href="activity.php"  class="btn btn-warning"><i class="fa fa-list"></i> Activity</a>

                   &nbsp; &nbsp;
                   <!-- edit profile -->
                   <a data-toggle="modal" href="#editProfileModal" class="btn btn-info">
                    <i class="fa fa-pencil"></i> Edit profile</a>
                    <br>
                </div>



                <!-- edit profile modal -->
                <div id="editProfileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title text-center" id="myModalLabel">Edit your profile</h4>
                            </div>
                            <div class="modal-body">

                                <!-- select image section-->
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="fileupload fileupload-new pull-left" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail">
                                                <!-- Avatar image -->
                                                <img id="avatar-img" class="img-responsive img-circle center-cropped-avatar" src="<?php echo $avatarURL ?>" height="64" width="64" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px; width: 120px"></div>

                                            <span class="btn btn-file btn-info">
                                            <span class="fileupload-new">Select image</span>
                                            <span class="fileupload-exists">Change</span>

                                            <!-- imageData input -->
                                            <input id="imageData" name="file" type="file" accept="image/*"/>
                                        </span>
                                    </div>
                                </div>
                                Upload an avatar
                            </div><!-- ./ select image section -->

                            <!-- automatically upload image script -->
                            <script>
                                document.getElementById("imageData").onchange = function () {
                                    var reader = new FileReader();
                                    reader.onload = function (data) {
                                        document.getElementById("avatar-img").src = data.target.result;
                                        console.log(data.target.result);
                                        document.getElementById("avatar-img").onload = function () {
                                            // Upload the selected image automatically into the 'uploads' folder
                                            var filename = "avatar.jpg";
                                            var data = new FormData();
                                            data.append('file', document.getElementById('imageData').files[0]);
                                            var websitePath = "<?php echo $_GLOBALS['WEBSITE_PATH'] ?>";

                                            $.ajax({
                                                url : "upload-avatar.php",
                                                type: 'POST',
                                                data: data,
                                                contentType: false,
                                                processData: false,
                                                success: function(data) {
                                                    console.log(websitePath + data);
                                                    // Set value in the input fileURLTxt
                                                    document.getElementById("fileURLTxt").value = websitePath + data;
                                                }, error: function(e) {
                                                    alert("Something went wrong, try again! " + e);
                                                }
                                            });
                                        };
                                    };
                                    if (document.getElementById('imageData').files[0]) {
                                        reader.readAsDataURL(document.getElementById('imageData').files[0]);
                                    }
                                };
                            </script>

                            <!-- Hidden frame to stay on this page -->
                            <iframe name="myframe" style="display:none;"></iframe>


                            <!-- form -->
                            <form action="edit-profile.php" target="myframe">
                                <div class="form-group">
                                    <!-- Fullname text input -->
                                    <strong>FULL NAME</strong>
                                    <input name="fullName" type="text" class="form-control" autocomplete="name" placeholder="Your full name" value="<?php echo $fullName ?>">
                                    <br>

                                    <!-- Email input -->
                                    <strong>EMAIL</strong>
                                    <input name="email" type="text" class="form-control" autocomplete="email" placeholder="Your email address" value="<?php echo $email ?>">
                                    <br>

                                    <!-- Hidden fileURL input -->
                                    <input id="fileURLTxt" style="opacity:0;" size="50" type="text" name="fileURL" value="">

                                    <!-- update profile button -->                          
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary text-center" onclick="showLoadingForEditProfile()"><em class="fa fa-floppy-o"></em> Update profile
                                        </button>
                                    </div>
                                </div>
                            </form><!-- ./ form -->

                        </div><!-- end modal body -->

                        <!-- cancel button -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div><!-- ./ edit profile modal -->
	

<!-- footer -->					
<?php include 'footer.php' ?>
   

<!-- Javascript functions -->
<script>
// LOGOUT -----------------------------------
function logOut() {
    // Show Loading modal
    document.getElementById("loadingText").innerHTML = "Logging out, please wait...";
    $('#loadingModal').modal('show');
    
        
    $.ajax({
        url:"logout.php",  
        
        success:function(data) {
            var results = data;  
            console.debug(results);

            window.location.href = "index.php";
           
        // error
        }, error: function () {
            alert('Something went wrong. Try again!');
        }
    });
}




// SHOW LOADING MODAL (FOR EDIT PROFILE)
function showLoadingForEditProfile() {
    // hide editProfile Modal
    $('#editProfileModal').modal('hide');

    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Updating your profile...";
    $('#loadingModal').modal('show');

    
    setTimeout(function(){
        location.reload(); 
    }, 1000);
}



// SHOW LOADING MODAL ------------------------------------
function showLoadingModal() {
    
    // Show loading modal
    $('#loadingModal').modal('show');

    
    setTimeout(function(){
        location.reload(); 
    }, 500);
        
}


</script>

</body>
</html>