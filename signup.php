<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
session_start();

ParseUser::logOut();

// SIGN UP ------------------------------------------------
if(	isset($_POST['username']) 
    && isset($_POST['password']) 
	&& isset($_POST['fullName'])
    && isset($_POST['email'])
){
	
    $username = $_POST['username'];
    $password = $_POST['password'];
    $fullName = $_POST['fullName'];
    $email = $_POST['email'];

	
if ($username != '' && $password != '' && $fullName != '' && $email != '') {
    
    $user = new ParseUser();
    $user->set("username", $username);
    $user->set("password", $password);
    $user->set("email", $email);
    $user->set("isReported", false);
    $user->set("fullName", $fullName);		

    
	// Saving block
  	try {
  		$user->signUp();

        // SAVE A FILE (an image)   
        $fileURL = $_POST['fileURL'];

        if ($fileURL != '') {
        	// get selected image
            $localFilePath = $fileURL;
        } else {
        	// Save default avatar
            $localFilePath = 'assets/img/user_avatar.png';
        }
            
        $file = ParseFile::createFromFile($localFilePath, "avatar.jpg");
        $file->save();
        // $url = $file->getURL();
        $user->set("avatar", $file);
        $user->save();
		

        echo '
			<center>
                <p><strong>
                    You have successfully signed up.
                    <br>
                    Please wait...
                </strong></p>
            </center>
            
            <script>location.href = "index.php";</script>
		';
	  
		// Go back to index.php
	  	header("Refresh:1; url=index.php");
	  	

	// error 
    } catch (ParseException $error) { $e = $error->getMessage();
      
      echo '
            <div class="alert alert-danger text-center">
            <em class="fa fa-exclamation"></em>
            '.$e.'
            </div>  
        '; 
    }
	
	
	
// You must fill all the fields to sign up!
} else {
	echo '
        <div class="alert alert-success text-center">
        You must fill all the fields to sign up!
        </div>
	';
}

}
?>

<!-- header -->
<?php include 'header.php'; ?>

<body>
    <div class="container">

        <!-- title -->
        <div class="text-center">
            <h2><img src="assets/img/80.png" width="28"> <a href="index.php">AskIt</a> <small>| A place for questions</small></h2>
        </div>
        <br><br><!-- ./ title -->

        <div class="row">
            <div class="col-lg-offset-4 col-md-offset-4 col-sm-offset-3 col-lg-4 col-md-4 col-sm-5 text-center">
                <h4>Sign up</h4>
                <br>

                <!-- select image section -->
                <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="fileupload fileupload-new pull-left" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" >
                                    
                                    <!-- Avatar image -->
                                    <img id="avatar-img" class="img-responsive img-circle center-cropped-avatar" src="assets/img/user_avatar.png" height="64" width="64" />
																		
                                </div>
                                        
                                <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px; width: 120px"></div>

                                        <span class="btn btn-file btn-info">
                                            <span class="fileupload-new">Select image</span>
                                            <span class="fileupload-exists">Change</span>
                                            
                                            <!-- ImageData input -->
                                            <input id="imageData" name="file" type="file" accept="image/*"/>

                                        </span>
                                    </div>
                                </div>

                            <div class="panel-footer">
                                <h5>Upload an Avatar</h5>
                  			</div>
                </div><!-- ./ select image section -->


			<!-- AUTOMATICALLY UPLOAD SELECTED IMAGE -->
            <script>
                document.getElementById("imageData").onchange = function () {
				    var reader = new FileReader();
				    
                    reader.onload = function (data) {
    				    document.getElementById("avatar-img").src = data.target.result;
    				    console.log(data.target.result);
    				    document.getElementById("avatar-img").onload = function () {
        		
        				    // Upload the selected image automatically into the 'uploads' folder
            				var filename = "avatar.jpg";
            				var data = new FormData();
            				data.append('file', document.getElementById('imageData').files[0]);
            				var websitePath = "<?php echo $_GLOBALS['WEBSITE_PATH'] ?>";

            				$.ajax({
                				url : "upload-avatar.php",
                				type: 'POST',
                				data: data,
                				contentType: false,
                				processData: false,
                				success: function(data) {
                    				console.log(websitePath + data);
                    				// Set value in the input fileURLTxt
                    				document.getElementById("fileURLTxt").value = websitePath + data;                     
                				}, error: function(e) {
                    				alert("Something went wrong, try again! " + e);
                				}
            				});
                        };
                    };
				    if (document.getElementById('imageData').files[0]) {
    				    reader.readAsDataURL(document.getElementById('imageData').files[0]);
				    }
                };								
			</script>

           

        <!-- sign up form -->
        <form action="signup.php" method="post">
            <div class="form-group">

                <!-- Hidden fileURL input -->
                <input id="fileURLTxt" style="opacity:0;" size="50" type="text" name="fileURL" value="">

                <div class="input-group input-icon">    
                    <span class="input-group-addon">
                        <em class="fa fa-user fa-fw"></em>
                    </span>

                    <!-- username input -->
                    <input type="text" name="username" class="form-control" placeholder="type your email address">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group input-icon">
                    <span class="input-group-addon">
                        <em class="fa fa-lock fa-fw"></em>
                    </span>

                    <!-- password input -->
                    <input type="password" name="password" class="form-control" placeholder="type a password">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group input-icon">
                    <span class="input-group-addon">
                        <em class="fa fa-envelope fa-fw"></em>
                    </span>

                    <!-- email input -->
                    <input type="email" name="email" class="form-control" placeholder="type your email">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group input-icon">
                    <span class="input-group-addon">
                        <em class="fa fa-user fa-fw"></em>
                    </span>

                    <!-- fullName input -->
                    <input type="text" name="fullName" class="form-control" placeholder="type your full name">
                </div>
            </div>

            <!-- Sign up button -->
            <input type="submit" value="Sign up" class="btn btn-primary btn-block" onclick="showLoadingModal()">

        </form><!-- ./ sign up form -->
        <br>

        <div class="text-center">
		  By signing up, you agree to our <a href="tou.html">Terms of Use</a> and that you have read our Data Privacy, including our Cookie Use.
        </div>
        <br><br>

    </div>
</div><!-- ./ row -->



<!-- footer -->
<?php include 'footer.php'; ?>


<script>
// SHOW LOADING MODAL ------------------------------------
function showLoadingModal() {
    // Show loading modal
    $('#loadingModal').modal('show');
}
</script>

</body>
</html>