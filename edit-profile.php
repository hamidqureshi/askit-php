<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
session_start();

    /* Variables */
    $currUser = ParseUser::getCurrentUser();
    
    $fullName = $_GET['fullName'];
    $email = $_GET['email'];
    
    $currUser->set('fullName', $fullName);
    $currUser->set('email', $email);

    // Save avatar
    $fileURL = $_GET['fileURL'];

    if ($fileURL != '') {
        $file = ParseFile::createFromFile($fileURL, "avatar.jpg");
        $file->save();
        // $url = $file->getURL();
        $currUser->set("avatar", $file);    
    }

    try {
        $currUser->save();

        echo 'Your profile has been updated';

    // error
    } catch ( ParseException $e){ echo $e->getMessage(); }
?>