<?php
ob_start();
?>
<!-- APP STORE BADGES -->
<div class="">
    <h6 class="text-center">
        Get the mobile versions here:
        <br><br>
        <a href="<?php echo $iosAppStoreLink ?>" target="_blank"><img src="assets/img/appstore-badge.png" width="140"></a>
        &nbsp; 
        <a href="<?php echo $googlePlayStoreLink ?>" target="_blank"><img src="assets/img/playstore-badge.png" width="140"></a>
    </h6>
</div>
<?php $appsHTML = ob_get_clean();

echo '
    <!-- loading modal -->
    <div id="loadingModal" class="modal fade" tabindex="-1"  data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <h5><i class="fa fa-spin fa-spinner"></i><br><br>
                        <p id="loadingText"> Loading...</p>
                        </h5>
                    </div>
                </div><!-- ./ modal body -->
    </div></div></div><!-- ./ loading modal -->


</div></div></div><!-- ./ container -->

	<hr>
	
	<!-- FOOTER -->
    <footer class="text-center">
        <p>
            Made with <i class="fa fa-heart"></i> by
            <a href="http://bit.ly/2PdQZBp" target="_blank">cubycode</a>
        </p>
        <p><a href="tos.html">Terms of Service</a></p>
        <br><br>

        '.$appsHTML.'
    </footer>
		
    <!-- Lihghtbox -->
    <script src="assets/lightbox/dist/js/lightbox-plus-jquery.min.js"></script>
    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <!-- Main Scripts-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Video -->
    <script src="assets/js/video.js"></script>
    <script src="assets/js/responsive.video.js"></script>
    <!-- Plan UI elements -->
    <script src="assets/js/plan.ui.js"></script>
    <!-- PlaceHolder fallback -->
    <script src="assets/js/jquery.placeholder.js"></script>
    <!-- Demo -->
    <script src="assets/js/application.js"></script>
';
?>