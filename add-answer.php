<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();


/* Variables */
$text = $_GET['text'];
$qObjID = $_GET['qObjID'];

// get Question Object
$qObj = new ParseObject("Questions", $qObjID);
$qObj->fetch();	
$qText = $qObj->get('text');

// Get Question's user
$qUser = $qObj->get('userPointer');
$qUser->fetch();
$qUserID = $qUser->getObjectId();


// Get current User 
$currUser = ParseUser::getCurrentUser();
$cuFullName = $currUser->get('fullName');


// SAVE ANSWER ----------------
try {
	$aObj = new ParseObject("Answers");
	$aObj->set('text', strip_tags($text));
	$aObj->set('userPointer', $currUser);
	$aObj->set('questionPointer', $qObj);
	$aObj->set('isReported', false);
	$aObj->setArray('likedBy', array());
	$aObj->set('likes', 0);
	$aObj->save();

	// Increment answers in the question Obj
	$qObj->increment('answers', 1);
	$qObj->save();


	// Send Push Notification
	$pushMessage = $cuFullName.' answered to your Question: '.$qText;
    $alert = array("alert" => $pushMessage);

    // Send Push to iOS and Android devices
    ParseCloud::run( "push", array("someKey" => $qUserID, "data" => $alert) );
    ParseCloud::run( "pushAndroid", array("someKey" => $qUserID, "data" => $alert) );


	// Save Activity			
	$actObj = new ParseObject("Activity");
	$actObj->set('currentUser', $qUser);
	$actObj->set('otherUser', $currUser);
	$actObj->set('text', $pushMessage);    
	$actObj->save();

	echo 'ANSWER SAVED!';

// error in query Ads
} catch (ParseException $e){ echo $e->getMessage(); }
?>