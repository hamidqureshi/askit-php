<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();


/* Variables */
$userID = $_GET['userID'];
$reportMessage = 'Offensive user';

// Get User Object
$userObj = new ParseUser("_User", $userID);
$userObj->fetch();

// Set isReported to true for this User
ParseCloud::run( "reportUser", array("userId" => $userID, "reportMessage" => $reportMessage) );


// 1. Query and Report all Questions of this user (if any)
try {
	$query = new ParseQuery("Questions");
    $query->equalTo('userPointer', $userObj);
        
    // find objects
    $qArray = $query->find();      
    for ($i = 0;  $i < count($qArray); $i++) {
    	$qObj = $qArray[$i];
        $qObj->set('isReported', true);
        $qObj->set('reportMessage', "*Automatically reported after User report*");
        $qObj->save();
    }

// error
} catch ( ParseException $e){ echo $e->getMessage(); }



// 2. Query and Report all Answers of this user (if any)
try {
	$query = new ParseQuery("Answers");
    $query->equalTo('userPointer', $userObj);
        
    // find objects
    $anArray = $query->find();      
    for ($i = 0;  $i < count($anArray); $i++) {
    	$aObj = $anArray[$i];
        $aObj->set('isReported', true);
        $aObj->set('reportMessage', "*Automatically reported after User report*");
        $aObj->save();
    }

// error
} catch ( ParseException $e){ echo $e->getMessage(); }
?>