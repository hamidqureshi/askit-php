<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();

// Open login.php in case current user is logged out
$currUser = ParseUser::getCurrentUser();
if ($currUser) {
} else { header("Refresh:0; url=login.php"); }
?>

<!-- header -->                 
<?php include 'header.php' ?>

<body>
    <div class="container">
        <!-- title -->
        <div>
            <h2><img src="assets/img/80.png" width="28"> <a href="index.php">AskIt</a> <small>| A place for questions</small></h2>
        </div>
        <br><!-- ./ title -->

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <!-- Lead title -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <p class="lead text-center">
                            <strong>Activity</strong>
                        </p>
                    </div>

<!-- activity section -->
<?php
    // Get current User
    $currUser = ParseUser::getCurrentUser();
    $cuObjectID = $currUser->getObjectId();

    // QUERY ACTIVITY ---------------------------------
    try {
        $query = new ParseQuery("Activity");
        $query->includeKey("_User");
        $query->equalTo("currentUser", $currUser);
        $query->descending("createdAt");
        
        // find objects
        $actArray = $query->find();   
                
        for ($i = 0;  $i < count($actArray); $i++) {
            // Get Parse object
            $actObj = $actArray[$i];
            
            // Get Activity text
            $actText = $actObj->get('text');
           
            // Get Activity date
            $actCreatedAt = $actObj->getCreatedAt();
            $actDate = date_format($actCreatedAt,"Y/m/d H:i:s");
            

            // Get otherUser Pointer
            $ouPointer = $actObj->get("otherUser");
            $ouPointer->fetch();
            // Get otherUser ObjectID
            $ouUserID = $ouPointer->getObjectId();

            // Get avatar
            $file = $ouPointer->get('avatar');
            $avatarURL = $file->getURL();        
            ?>

            <!-- activity row-->
            <div class="list-group">
                <div class="list-group-item">
                    <div class="valign">
                        <div class="cell-1-4">
                            <img class="img-responsive img-circle center-cropped-avatar" src="<?php echo $avatarURL ?>">
                        </div>
                        <div style="width: 100%; height: auto">
                            <p><?php echo $actText ?></a></p>
                            <p><i class="fa fa-clock"></i><?php echo time_ago($actDate) ?></p>
                        </div>
                    </div>
                </div>
            </div><!-- ./ activity row -->     
            
        <?php } 
 
     // error in query
    } catch (ParseException $e){ echo $e->getMessage(); }
?>
    								


<!-- footer -->                 
<?php include 'footer.php' ?>	 
	 
</body>
</html>