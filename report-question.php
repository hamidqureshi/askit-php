<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();


/* Variables */
$qObjID = $_GET['qObjID'];
$reportMessage = 'Inappropriate Question';

// Get Question Object
$qObj = new ParseObject("Questions", $qObjID);
$qObj->fetch();

$qObj->set('isReported', true);
$qObj->set('reportMessage', $reportMessage);
        
try {
    $qObj->save();

// error
} catch ( ParseException $e){ echo $e->getMessage(); }
?>