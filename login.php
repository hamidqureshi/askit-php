<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
session_start();


// LOGIN ------------------------------------------
if(isset($_POST['username']) && isset($_POST['password']) ) {

    $username = $_POST['username'];
    $password = $_POST['password'];
        
    try {
        $user = ParseUser::logIn($username, $password); 
                
        echo '
            <center>
                <p><strong>
                    You have successfully logged in. <br>
                    Please wait...
                </strong></p>
            </center>
            
            <script>location.href = "index.php";</script>
        ';

    // error 
    } catch (ParseException $error) { $e = $error->getMessage();
        echo '
            <div class="text-center">
                <div class="alert alert-danger">
                    <em class="fa fa-exclamation"></em>'.$e.'
                </div>  
            </div>
        '; 
    }
}




// FORGOT PASSWORD -----------------------------------
if( isset($_POST['email']) ) {

    $email = $_POST['email'];

    try {
        ParseUser::requestPasswordReset($email);
    
        echo '
            <div class="alert alert-success">
                Cool, you will get email shortly with a link to reset your password!
            </div>  
        '; 

    // error
    } catch (ParseException $error) { $e = $error->getMessage();
        echo '
            <div class="alert alert-danger">
            <em class="fa fa-exclamation"></em>
                '.$e.'
            </div>  
        '; 
    }
}
?>

<!-- header -->
<?php include 'header.php'; ?>

<body>
    <div class="container">

        <!-- title -->
        <div class="text-center">
            <h2><img src="assets/img/80.png" width="28"> <a href="index.php">AskIt</a> <small>| A place for questions</small></h2>
        </div>
        <br><br><!-- ./ title -->

        <div class="row">
            <div class="col-lg-offset-4 col-md-offset-4 col-sm-offset-3 col-lg-4 col-md-4 col-sm-5 text-center">

                <!-- Login form with error -->
                <div class="panel panel-primary">
                    <div class="panel-body">                       
                        <h4>Log in</h4>
                        <br>
                        <!-- form -->
                        <form class="" action="login.php" method="post">
                            
                            <!-- Username input -->
                            <div class="form-group">
                                <div class="input-group input-icon">                                    
                                    <span class="input-group-addon">
                                        <em class="fa fa-user fa-fw"></em>
                                    </span>                            
                                    <input type="text" name="username" class="form-control" placeholder="username">
                                </div>
                            </div>
                                             
                            <!-- Password input -->   
                            <div class="form-group">
                                <div class="input-group input-icon">
                                    <span class="input-group-addon">
                                        <em class="fa fa-lock fa-fw"></em>
                                    </span>
                                    <input type="password" name="password" class="form-control" placeholder="password">
                                </div>
                            </div>
                                                       
                            <!-- Login button -->
                            <input type="submit" value="Log in" class="btn btn-primary btn-block">
                        </form><!-- ./ form -->
                    </div>
                    <br><br>
										
                    <!-- sign up button -->
                	<div><a class="btn btn-default" href="signup.php"> Sign up with email </a></div>
                	<br>


<!-- facebook login button -->
<div>

<?php
require_once 'fb-autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

$fb = new Facebook\Facebook([
  'app_id'                => $_GLOBALS["FACEBOOK_APP_ID"],
  'app_secret'            => $_GLOBALS["FACEBOOK_APP_SECRET"],
  'default_graph_version' => 'v2.3',
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl($GLOBALS['WEBSITE_PATH'].'fb-callback.php', $permissions);

echo '
    <a class="btn btn-info" href="'.htmlspecialchars($loginUrl).'">
    <i class="fa fa-facebook"></i> Sign in with Facebook</a>
';

?>
    </div>
    <br>

    <!-- forgot password -->
    <div class="panel-footer">
        <div class="text-center">
            <a data-toggle="modal" href="#forgotModal">
                <strong>Forgot your password?</strong>
            </a>
        </div>
    </div>
    </div>
    <br>

    <!-- app store badges -->
    <div class="col-lg-12 col-sm-12">
        <h6 class="text-center">
            Get the mobile versions here:
            <br><br>
            <a href="<?php echo $iosAppStoreLink ?>" target="_blank"><img src="assets/img/appstore-badge.png" width="140"></a>
                &nbsp; 
            <a href="<?php echo $googlePlayStoreLink ?>" target="_blank"><img src="assets/img/playstore-badge.png" width="140"></a>
        </h6>
    </div>
    <br>


    <!-- forgot password modal -->
    <div id="forgotModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="myModalLabel">Reset Password</h4>
                </div>
                <div class="modal-body">          
                    <div align="center">
                        <p>Type the email adress you used to sign up in order to receive an email to reset your password</p>
                        <div class="input-group input-icon">
                        <!-- form -->
                        <form class="form-inline" action="login.php" method="post">
                            <div class="input-group">
                                <!-- email input -->
                                <input type="email" name="email" class="form-control" placeholder="email">
                                <!-- reset button -->
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Reset password</button>
                                </span>
                            </div>
                        </form><!-- ./ form -->

                        </div>
                    </div>
                </div><!-- end modal body -->

                <!-- cancel button -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div><!-- ./ forgot password modal -->


</div></div></div><!-- ./ container -->


<!-- footer -->
<?php include 'footer.php'; ?>

</body>
</html>