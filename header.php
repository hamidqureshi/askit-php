<?php
function roundNumbersIntoKMGT($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));

    // is this a number?
    if(!is_numeric($n)) return false;

    // now filter it;
    if($n>1000000000000) return round(($n/1000000000000),1).'T';
    else if($n>1000000000) return round(($n/1000000000),1).'G';
    else if($n>1000000) return round(($n/1000000),1).'M';
    else if($n>1000) return round(($n/1000),1).'K';

    return number_format($n);
}

// TIME AGO SINCE DATE -------------------------
function time_ago($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
echo '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>AskIt | A place for questions</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="questions answers php web template">
    <meta name="author" content="cubycode">

  	<!-- Favicon -->
   	<link rel="shortcut icon" type="image/png" href="assets/img/80.png"/>

    <!-- lightbox -->
    <link rel="stylesheet" href="assets/lightbox/dist/css/lightbox.min.css">

    <!-- Google Web Font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,700,300italic" rel="stylesheet" type="text/css">

    <!-- Bootstrap 3 Plan styles -->
    <link href="assets/css/bootstrap.main.css" rel="stylesheet">
    <!-- Plan UI -->
    <link href="assets/css/main.css" rel="stylesheet">
    <!-- Vide JS -->
    <link href="assets/css/video-js.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
    $(function () {
        $("[data-toggle=\'tooltip\']").tooltip();
      })
    </script>

    <style> 
		body { 
			padding-top: 10px; 
		} 
		hr { 
			margin: 60px 0; 
		} 
		
		.line { 
		    display: block;
		    color: #999999;
		    margin-top: 0.5em;
		    margin-bottom: 0.5em;
		    margin-left: auto;
		    margin-right: auto;
		    border-style: inset;
		    border-width: 1px;
		}
		
		.center {
  		position: absolute;
  		left: 0;
  		right: 0;
  		margin: auto; 
    } 
		
	/* Center and crop an image */
    .center-cropped-img {
            width: 100%;
            height: 150px;
            background-position: center center;
            background-repeat: no-repeat;
            object-fit: cover;
            border-radius: 6px;
     }

    /* Center and crop an avatar */
    .center-cropped-avatar {
            width: 50px;
            height: 50px;
            background-position: center center;
            background-repeat: no-repeat;
            object-fit: cover;
    }

    /* Center and crop an avatar */
    .center-cropped-avatar-80 {
            width: 80px;
            height: 80px;
            background-position: center center;
            background-repeat: no-repeat;
            object-fit: cover;
    }

    @media screen and (max-width: 500px) {
            .button-text {
                display: none;
            }
    }
    </style>
    
    
</head>
';


function excerpt($text, $max_length = 140, $cut_off = '...'){
    
    if(strlen($text) <= $max_length) {
        return $text;
    }

    if(strlen($text) > $max_length) {
    
        $text = substr($text, 0, $max_length);
        $text = rtrim($text);
        $text .=  $cut_off;

    }

    return $text;
}
?>