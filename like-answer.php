<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();



/* Variables */
$aObjID = $_GET['aObjID'];
	
// Get current User 
$currUser = ParseUser::getCurrentUser();
$currUserID = $currUser->getObjectId();
$cuFullName = $currUser->get("fullName");

// Get Answer Object
$aObj = new ParseObject("Answers", $aObjID);
$aObj->fetch();
$aText = $aObj->get('text');

// Get userPointer
$userPointer = $aObj->get("userPointer");
$userPointer->fetch();
$upUserID = $userPointer->getObjectId();

// Get Question Pointer
$questPointer = $aObj->get("questionPointer");
$questPointer->fetch();

// Get likedBy
$likedBy = $aObj->get("likedBy");

// Unlike Answer
if (in_array($currUserID, $likedBy)) {
	try {
		$likedBy = remove_element($likedBy, $currUserID);
		$aObj->setArray('likedBy', $likedBy);
			
		// Subtract 1 like to the answer
		$aObj->increment("likes", -1);
		$aObj->save();

		// echo updated likes
		echo 'UNLIKE';

	// error
	} catch (ParseException $e){ echo $e->getMessage(); }


// Like Answer ----------------
} else {
	try {
		array_push($likedBy, $currUserID);
		$aObj->setArray('likedBy', $likedBy);

		// Add 1 like to the Answer
		$aObj->increment("likes", 1);
		$aObj->save();
			

		// Send Push Notification
        $pushMessage = $cuFullName.' liked your Answer: '.$aText;
        $alert = array("alert" => $pushMessage);

        // Send Push to iOS and Android devices
        ParseCloud::run( "push", array("someKey" => $upUserID, "data" => $alert) );
        ParseCloud::run( "pushAndroid", array("someKey" => $upUserID, "data" => $alert) );
			
		// Save Activity			
		$actObj = new ParseObject("Activity");
		$actObj->set('currentUser', $userPointer);
		$actObj->set('otherUser', $currUser);
		$actObj->set('text', $pushMessage);    
		$actObj->save();

		// echo updated likes
		echo 'LIKE';		  
	// error
	} catch (ParseException $e){ echo $e->getMessage(); }
}
  


function remove_element($array,$value) {
	foreach (array_keys($array, $value) as $key) {
		unset($array[$key]);
	}  
return $array;
}
?>