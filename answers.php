<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
// session_start();
?>
<!-- header -->
<?php include 'header.php'; ?>

<body>
	<div class="container">

        <!-- title -->
        <div class="col-lg-4 col-sm-4">
            <h2><img src="assets/img/80.png" width="28"> <a href="index.php">AskIt</a> <small>| A place for questions</small></h2>
        </div><!-- ./ title -->

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <!-- justified navbar -->
                <div class="navbar navbar-default navbar-justified">

                    <ul class="nav navbar-nav">
                        <!-- back button -->
                        <li>
                            <a href="javascript:history.go(-1)">
                            <em class="fa fa-arrow-left"></em> <span class="button-text"> Back</span></a>
                        </li>

                        <!-- add answer button -->
                        <li>
                            <?php
                                $currentUser = ParseUser::getCurrentUser();
                                if ($currentUser) {
                                    echo '
                                        <a data-toggle="modal" href="#addAnswerModal">
                                        <em class="fa fa-plus"></em> <span class="button-text"> Answer</span></a>
                                    ';
                                } else {
                                     echo '
                                        <a href="'.$GLOBALS['WEBSITE_PATH'].'login.php">
                                        <em class="fa fa-plus"></em> <span class="button-text"> Answer</span></a>
                                    ';
                                }
                            ?>
                        </li>

                        <!-- account button -->
                        <li>
                            <?php
                                  $currentUser = ParseUser::getCurrentUser();
                                  if ($currentUser) {
                     		         echo '<a href="'.$GLOBALS['WEBSITE_PATH'].'account.php">';
                                    } else {
                                        echo '<a href="'.$GLOBALS['WEBSITE_PATH'].'login.php">';
                                    }
							?>
                            <em class="fa fa-user"></em> <span class="button-text"> Account</span>
							</a>
                        </li>
                    </ul>

                </div>
            </div><!-- ./ justified navbar -->


            <div class="row aaa">
                <div class="col-lg-12 col-md-12 col-sm-12">
<?php

    /* Variables */
    $qObjID = $_GET['qObjID'];
    
    $currUser = ParseUser::getCurrentUser();
    $currUserID = $currUser->getObjectId();

    // Get Question Obj
    $qObj = new ParseObject('Questions', $qObjID);
    $qObj->fetch();
    $file = $qObj->get('image');
    $qText = $qObj->get('text');

    $defaultBanner = "https://". $_SERVER['SERVER_NAME']."/askit/assets/img/default-banner.png";

    echo '
        <h4 class="text-center"><em>'.$qText.'</em></h4>
    ';

    if ($file != null) {
        $imageURL = $file->getURL();

    ?>
    <div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3" style="padding-bottom:15px;">
        <br>
        <a href="<?php echo $imageURL ?>" data-lightbox="images">
        <img class="center-cropped-img" src="<?php echo $imageURL ?>"></a>
    <br>
    </div>
    <?php } else {
        ?>
            <br><br>
            <a href="<?php echo $defaultBanner ?>" data-lightbox="images">
            <img class="center-cropped-img" src="<?php echo $defaultBanner ?>"></a>
            <br>
            </div>
            <?php 

    } 

    // QUERY ANSWERS ---------------------------------
    try {
        $query = new ParseQuery("Answers");
        $query->includeKey("_User"); 
        $query->equalTo("questionPointer", $qObj);
        $query->equalTo("isReported", false);
        $query->descending('createdAt');
        $query->limit(10000);

        // Execute query
        $aArray = $query->find();   
        for ($i = 0;  $i < count($aArray); $i++) {
            
            // Get Parse Object
            $aObj = $aArray[$i];
            $aObjID = $aObj->getObjectId();
            
            // Get Row Nr
            $rowNr = $i;

            // Get text
            $text = $aObj->get('text');

            // Get date and format it
            $date = $aObj->getCreatedAt();
            $aDate = date_format($date,"Y/m/d H:i:s");

            // Get LikedBy
            $likedBy = $aObj->get('likedBy');
           
            // Get userPointer -------
            $userPointer = $aObj->get("userPointer");
            $userPointer->fetch();
            // Get user's details
            $username = $userPointer->get('username');
            $fullName = $userPointer->get('fullName');
            $userID = $userPointer->getObjectId();

            // Get Avatar
            $defaultAvatar = "https://". $_SERVER['SERVER_NAME']."/askit/assets/img/default-user.png";
            $defaultBanner = "https://". $_SERVER['SERVER_NAME']."/askit/assets/img/default-banner.png";
            
            
            $fileAvatar = $userPointer->get('avatar');
            if ($fileAvatar) {
                $avatarURL = !empty($fileAvatar->getURL()) ? $fileAvatar->getURL() : $defaultAvatar;
            } else {
                $avatarURL = $defaultAvatar;
            }
            

            echo '
                <!-- answer cell -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                          
                        <!-- User details -->        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <img class="img-circle center-cropped-avatar" src="'.$avatarURL.'">
                                &nbsp;
                                                                
                                <!-- full name and date -->
                                <strong>'.$fullName.'</strong> | '.time_ago($aDate).'

                                <!-- options toggle  -->
                                <a data-toggle="modal" class="pull-right" href="#optionsModal" onclick="setOptions(\''.$fullName.'\', \''.$userID.'\', \''.$aObjID.'\')">
                                <i class="fa fa-chevron-down"></i></a>
                                <br><br>

                                <!-- answer text -->
                                <p>'.$text.'</p>

                                <!-- like button -->
                                <a id="likeButt'.$rowNr.'" href"#" onclick="likeAnswer(\''.$aObjID.'\', \''.$rowNr.'\')">';
                                if (in_array($currUserID, $likedBy)) {
                                    echo '<i class="fa fa2x fa-heart pull-right" style="color: red; font-size: 20px"></i>';
                                } else { echo '<i class="fa fa2x fa-heart-o pull-right" style="font-size: 20px"></i>'; }
                                echo '
                                    </a>    
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div><!-- ./ answer cell -->
            ';
            
        } // end FOR loop
 
    // error in query
    } catch (ParseException $e){ echo $e->getMessage(); }
?>
        
        </div>
    </div><!-- ./ answers section -->



    <!-- add question modal -->
    <div id="addAnswerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="myModalLabel">Add Answer</h4>
                </div>
                <div class="modal-body">

                    <!-- Hidden frame to stay on this page -->                                
                    <iframe name="myframe" style="display:none;"></iframe>
           
                    <!-- form -->
                    <form action="add-answer.php" target="myframe">
                        <div class="form-group">
                            <textarea name="text" rows="3" class="form-control" placeholder="Your answer ..."></textarea>
                        </div>
                        
                         <!-- Hidden qObjID input -->
                        <input id="qObjID" style="opacity:0;" size="15" type="text" name="qObjID" value="<?php echo $qObjID ?>">

                        <div class="clearfix">
                            <div class="text-center">
                                <input type="submit" value="Post answer" class="btn btn-primary btn-block" onclick="showLoadingModal2()" >
                            </div>
                        </div>
                    </form><!-- ./ form --> 
                </div><!-- ./ modal body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div><!-- ./ add question modal -->


    <!-- options modal -->
    <div id="optionsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <h4 class="modal-title text-center" id="myModalLabel">Select option</h4>
                    </div>

                    <!-- report user button -->
                    <div id="reportUserButt">report user</div>
                    <div class="line"></div>
                                        
                    <!-- report answer button -->
                    <div id="reportAnswerButt">report answer</div>
                    <div class="line"></div>

                </div><!-- ./ modal body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div><!-- ./ options modal -->



<!-- footer -->
<?php include 'footer.php'; ?>
	 

<!-- Javascript function -->
<script>
    
// LIKE ANSWER FUNCTION -------------------                                      
function likeAnswer(aObjID, rowNr) {  
    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Please wait...";
    $('#loadingModal').modal('show');

    $.ajax({
        url:"like-answer.php?aObjID=" + aObjID,  
        type: "GET", 

        success:function(data) {
            var results = data.replace(/\s+/, ""); //remove any white spaces from the returned string
            console.log(results);

            // Reload page
            setTimeout(function(){
                location.reload();
            }, 100);
    }});
}


// SET OPTIONS ------------------------------------
function setOptions(fullName, userID, aObjID) {
    // REPORT USER BUTTON
    document.getElementById("reportUserButt").innerHTML = "<a id='reportUserButt' href='javascript:reportUser(\"" + userID + "\");'> <h5>Report @" + fullName + "<span style='opacity: 0'> -- " + userID + "</h5></a>";
    
    // REPORT QUESTION BUTTON   
    document.getElementById("reportAnswerButt").innerHTML = "<a id='reportAnswerButt' href='javascript:reportAnswer(\"" + aObjID + "\");'> <h5>Report Answer <span style='opacity: 0'> —- " + aObjID + "</h5></a>";
    
}


// REPORT A USER ---------------------------------------
function reportUser(userID) {
    $('#optionsModal').modal('hide');

    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Reporting User, please wait...";
    $('#loadingModal').modal('show');

    $.ajax({
            url:"report-user.php?userID=" + userID, 
            type: "GET", 
            
            success:function(data) {
                var results = data.replace(/\s+/, ""); //remove any trailing white spaces from returned string
                console.debug(results);

                $('#loadingModal').modal('hide');

                alert("Thanks for reporting this user! We will check it out within 24h.");

                // Reload page
                setTimeout(function(){
                    location.reload();
                }, 100);

            // error
            }, error: function (e) { alert(e);}
        });
}




// REPORT AN ANSWER  ---------------------------------------
function reportAnswer(aObjID) {
    $('#optionsModal').modal('hide');

    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Reporting Answer, please wait...";
    $('#loadingModal').modal('show');


    $.ajax({
            url:"report-answer.php?aObjID=" + aObjID, 
            type: "GET", 
            
            success:function(data) {
                var results = data.replace(/\s+/, ""); //remove any trailing white spaces from returned string
                console.debug(results);

                $('#loadingModal').modal('hide');

                alert("Thanks for reporting this Answer! We will check it out within 24h.");

                // Reload page
                setTimeout(function(){
                    location.reload();
                }, 100);

            // error
            }, error: function (e) { alert(e);}
        });
}



// SHOW LOADING MODAL
function showLoadingModal() {
    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Please wait...";
    $('#loadingModal').modal('show');
}




function showLoadingModal2() {
    // Hide Add Question modal
    $('#addAnswerModal').modal('hide');

    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Posting your Answer, please wait...";
    $('#loadingModal').modal('show');


    // Reload page
    setTimeout(function(){
        location.reload();
    }, 1000);
}




// ROUND LARGE NUMBER JS FUNCTION -----------------------------
function roundLargeNumbers(number, decPlaces) {
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10,decPlaces);

    // Enumerate number abbreviations
    var abbrev = [ "K", "M", "B", "T" ];

    // Go through the array backwards, so we do the largest first
    for (var i=abbrev.length-1; i>=0; i--) {

        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10,(i+1)*3);

        // If the number is bigger or equal do the abbreviation
        if(size <= number) {
             // Here, we multiply by decPlaces, round, and then divide by decPlaces.
             // This gives us nice rounding to a particular decimal place.
             number = Math.round(number*decPlaces/size)/decPlaces;

             // Handle special case where we round up to the next abbreviation
             if((number == 1000) && (i < abbrev.length - 1)) {
                 number = 1;
                 i++;
             }

             // Add the letter for the abbreviation
             number += abbrev[i];

             // We are done... stop
             break;
        }
    }

    return number;
}
	
</script>

</body>
</html>