<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}

// Redirect to login.php in case of User NOT logged in
$currUser = ParseUser::getCurrentUser();
if ($currUser == null) { header('Refresh:0; url=login.php'); }
?>

<!-- header -->
<?php include 'header.php'; ?>

<body>
    <div class="container">
	
		<!-- title -->
        <div class="col-lg-4 col-sm-4">
            <h2><img src="assets/img/80.png" width="28"> AskIt <small>| A place for questions</small></h2>
        </div><!-- ./ title -->



        <br>



        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">


                <!-- JUSTIFIED NAVBAR -->
                <div class="navbar navbar-default navbar-justified">
                    <ul class="nav navbar-nav">
                        

                         <!-- LATEST QUESTIONS BUTTON -->
                        <li>
                            <a href="index.php" onclick="showLoadingModal()">
                            <em class="fa fa-refresh"></em> <span class="button-text"> Latest 50</span></a>
                        </li>

                        <!-- SEARCH BUTTON -->
                        <li>
                            <a data-toggle="modal" href="#searchModal">
                            <em class="fa fa-search"></em> <span class="button-text"> Search</span></a>
                        </li>


                        <!-- ADD QUESTION BUTTON -->
                        <li>
                            <?php
                                $currentUser = ParseUser::getCurrentUser();
                                if ($currentUser) {
                                    echo '
                                        <a data-toggle="modal" href="#addQuestionModal">
                                        <em class="fa fa-plus"></em> <span class="button-text"> Add Question</span></a>
                                    ';
                                } else {
                                     echo '
                                     	<a href="'.$GLOBALS['WEBSITE_PATH'].'login.php">
										<em class="fa fa-plus"></em> <span class="button-text"> Add Question</span></a>
									';
                                }
                            ?>
                        </li>



                        <!-- ACCOUNT BUTTON -->
                        <li>
                            <?php
                                $currentUser = ParseUser::getCurrentUser();
                                if ($currentUser) {
                                    echo '<a href="'.$GLOBALS['WEBSITE_PATH'].'account.php">';
                                } else {
                                    echo '<a href="'.$GLOBALS['WEBSITE_PATH'].'login.php">';
                                }
							?>

                            <em class="fa fa-user"></em> <span class="button-text"> Account</span>
							</a>
                        </li>
                        

                    </ul>

                </div></div><!-- END JUSTIFIED NAVBAR -->
														
														

                


						
<!-- questions section -->
    <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
       		
<?php
    /* Variables */
    $keywords = $_GET['keywords'];
    
    // Make an array of keywords
    $keywArr = preg_split('/\s+/', $keywords);


    // QUERY QUESTIONS ---------------------------------
    try {
        $query = new ParseQuery("Questions");
        $query->includeKey("_User");
        
        // Search by Keywords
        if ($keywords != '') { $query->containedIn("keywords", $keywArr); }
        
        $query->equalTo("isReported", false);
        $query->descending('createdAt');
        $query->limit(50);
        

        // Execute query
        $qArray = $query->find(); 
        
        if ($qArray) {

        for ($i = 0;  $i < count($qArray); $i++) {
            
            // Get Parse Object
            $qObj = $qArray[$i];
            $qObjID = $qObj->getObjectId();
            
            // Get Row Nr
            $rowNr = $i;

            // Get text
            $text = $qObj->get('text');

            // Get date and format it
            $date = $qObj->getCreatedAt();
            $qDate = date_format($date,"Y/m/d H:i:s");

            // Get answers
            if ($qObj->get('answers') != null){
            	$answers = $qObj->get('answers');
			} else { $answers = '0';}

            // Get likes
			if ($qObj->get('likes') != null){
            	$likes = $qObj->get('likes');
			} else { $likes = '0';}
						
            // Get image file (if it exists)
            $file = $qObj->get('image');
            
            // Get likedBy array
            $likedBy = $qObj->get('likedBy');

          
            // Get userPointer -------
            $userPointer = $qObj->get("userPointer");
            $defaultAvatar = "https://". $_SERVER['SERVER_NAME']."/askit/assets/img/default-user.png";
            $defaultBanner = "https://". $_SERVER['SERVER_NAME']."/askit/assets/img/default-banner.png";

            if ($userPointer) {
                $userPointer->fetch();
                // Get user's details
                $username = $userPointer->get('username');
                $fullName = $userPointer->get('fullName');
                $userID = $userPointer->getObjectId();  
                
                $avatarURL  = (!empty($userPointer->get('avatar'))) ? $userPointer->get('avatar')->getURL() : $defaultAvatar;
            }

           


            // Get Avatar
            
?>
        

        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-body">

                    <!-- User details -->        
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-lg-12">
                            <img class="img-circle center-cropped-avatar" src="<?php echo $avatarURL ?>">
                            &nbsp;

                            <!-- full name and date -->
                            <strong><?php echo $fullName ?></strong> | <?php echo time_ago($qDate) ?>

                            <!-- options toggle -->
                            <a data-toggle="modal" class="pull-right" href="#optionsModal" onclick="setOptions('<?php echo $fullName ?>', '<?php echo $userID ?>', '<?php echo $qObjID ?>')">
                                <i class="fa fa-chevron-down"></i></a>

                                <!-- attached image -->
                                <?php 
                                if ($file != null) {
                                    $imageURL = $file->getURL();

                                ?>
                                    <br><br>
                                    <a href="<?php echo $imageURL ?>" data-lightbox="images">
                                    <img class="center-cropped-img" src="<?php echo $imageURL ?>"></a>
                                <?php } else {
                                    ?>
                                        <br><br>
                                        <a href="<?php echo $defaultBanner ?>" data-lightbox="images">
                                        <img class="center-cropped-img" src="<?php echo $defaultBanner ?>"></a>
                                        
                                        <?php 

                                } ?>



								<!-- question text -->
                                <h5><a style="text-decoration:none;" data-toggle="tooltip" href="answers.php?qObjID=<?php echo $qObjID; ?>"><?php echo excerpt($text,80) ?></a></h5>
                          
                            
                            </div>
                        </div>
                        <br>
												
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                            
                            <?php 
                            $currUser = ParseUser::getCurrentUser();
                            $currUserID = $currUser->getObjectId();

                            //like button
                            if (in_array($currUserID, $likedBy)) {
                                echo '<button id="likeButt'.$rowNr.'" class="btn btn-primary" ';
                            } else {
                                echo '<button id="likeButt'.$rowNr.'" class="btn btn-default" ';
                            }
                            echo '
                                    onclick="likeQuestion(\''.$qObjID.'\', \''.$rowNr.'\')"><i class="fa fa-heart"></i>
                                    <span class="p" id="likesNumb'.$rowNr.'"> &nbsp; '.roundNumbersIntoKMGT($likes).'</span>
                                    </button>

                                    &nbsp;&nbsp; 

                                    <!-- answer button --> 
                                    <a href="answers.php?qObjID='.$qObjID.'" class="btn btn-info"><i class="fa fa-comments-o"></i>&nbsp;  '.roundNumbersIntoKMGT($answers).'</a>
                                    	</div>
                                	</div>

                			</div></div></div><!-- ./ question cell -->
                			';

        } // end FOR loop

    } else {
        ?>
       
            <div class="col-md-12">
                <div class="alert alert-info">Opps! something went wrong, please try again </div>
            </div>
        

        <?php
    }
 
        
    // error in query
    } catch (ParseException $e){ //echo $e->getMessage(); 
    }


?>

    </div></div><!-- ./ questions section -->




				        
								
								
	<!-- search modal -->
    <div id="searchModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="myModalLabel">Search Questions</h4>
                </div>
                
                <div class="modal-body">
                                
                    <form action="index.php">
                        <div class="form-group">
                            <input type="text" name="keywords" class="form-control" placeholder="Search questions...">
                        </div>
                        
                        <div class="clearfix">
                            <div class="text-center">
                                <input type="submit" value="Search" class="btn btn-primary btn-block" onclick="showLoadingModal()" >
                            </div>
                        </div>
                    </form>
                                
                </div><!-- end modal body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>


    </div></div></div><!-- ./ serach modal -->
								
								






	<!-- OPTIONS MODAL -->
    <div id="optionsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">

                	<div class="modal-header">
                    	<h4 class="modal-title text-center" id="myModalLabel">Select option</h4>
                	</div>

                	<!-- REPORT USER BUTTON -->
                	<div id="reportUserButt">report user</div>
					<div class="line"></div>
										
					<!-- REPORT QUESTION BUTTON -->
					<div id="reportQuestionButt">report question</div>
					<div class="line"></div>
										
										
                </div><!-- end modal body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>


    </div></div></div><!-- END OPTIONS MODAL -->
		
		


    <!-- add question modal -->
    <div id="addQuestionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="myModalLabel">Add a Question</h4>
                </div>
                
                <div class="modal-body">
                    
                    <!-- Hidden frame to stay on this page -->                                
                    <iframe name="myframe" style="display:none;"></iframe>
                    
                    

                    <!-- SELECT IMAGE SECTION-->
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="fileupload fileupload-new pull-left" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" >
                                    
                                    <!--  image -->
                                    <img id="image" class="img-responsive center-cropped-img" src="assets/img/add_image.jpg" height="64" width="64" />
                                                                        
                                </div>
                                        
                                <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px; width: 120px"></div>

                                        <span class="btn btn-file btn-info">
                                            <span class="fileupload-new">Add image (optional)</span>
                                            <span class="fileupload-exists">Change</span>
                                            
                                            
                                            <!-- ImageData input -->
                                            <input id="imageData" name="file" type="file" accept="image/*"/>
                                            

                                        </span>
                                        <!-- <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a> -->
                                    </div>
                                </div>

                </div><!-- END SELECT IMAGE SECTION -->




            <!-- AUTOMATICALLY UPLOAD SELECTED IMAGE -->
            <script >
            

            document.getElementById("imageData").onchange = function () {
                var reader = new FileReader();
                reader.onload = function (data) {

                    document.getElementById("image").src = data.target.result;
                    console.log(data.target.result);
                    document.getElementById("image").onload = function () {
                
                        // Upload the selected image automatically into the 'uploads' folder
                        var filename = "image.jpg";
                        var data = new FormData();
                        data.append('file', document.getElementById('imageData').files[0]);

                        var websitePath = "<?php echo $_GLOBALS['WEBSITE_PATH'] ?>";

                        $.ajax({
                            url : "upload-image.php",
                            type: 'POST',
                            data: data,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                console.log(websitePath + data);
                                
                                // Set value in the input fileURLTxt
                                document.getElementById("fileURLTxt").value = websitePath + data;                     

                            }, error: function(e) {
                                alert("Something went wrong, try again! " + e);
                            }
                        });
                    };
                };
                if (document.getElementById('imageData').files[0]) {
                    reader.readAsDataURL(document.getElementById('imageData').files[0]);
                }
            };
                                                
            </script>

           
                    <!-- FORM -->
                    <form action="add-question.php" target="myframe">
                        <div class="form-group">
                            <textarea name="text" rows="3" class="form-control" placeholder="Your question ..."></textarea>
                        </div>
                        
                         <!-- Hidden fileURL input -->
                        <input id="fileURLTxt" style="opacity:0;" size="50" type="text" name="fileURL" value="">


                        <div class="clearfix">
                            <div class="text-center">
                                <input type="submit" value="Post question" class="btn btn-primary btn-block" onclick="showLoadingModal2()" >
                            </div>
                        </div>
                    </form>
                                
                </div><!-- end modal body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>


    </div></div></div><!-- ./ add question modal -->

	

<!-- footer -->
<?php include 'footer.php'; ?>


<!-- Javascript functions -->
<script>

// LIKE QUESTION FUNCTION -------------------                                      
function likeQuestion(qObjID, rowNr) {  
    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Please wait...";
    $('#loadingModal').modal('show');

    $.ajax({
        url:"like-question.php?qObjID=" + qObjID,  
        type: "GET", 

        success:function(data) {
            var results = data.replace(/\s+/, ""); //remove any white spaces from the returned string
            console.log(results);

            // Format results to split LIKE and LIKES AMOUNT
            var res = results.split("-");

            // Hide loading modal
            $('#loadingModal').modal('hide');

            if (res[0] == "LIKE") {
                $( "#likeButt" + rowNr).removeClass( "btn-default" ).addClass( "btn-primary" );

                document.getElementById('likesNumb' + rowNr).innerHTML = "&nbsp;&nbsp;" + roundLargeNumbers(res[1], 2);
                // console.log("UPDATED LIKES: " + res[1]);
                
            } else if (res[0] == "UNLIKE") {
                $( "#likeButt" + rowNr).removeClass( "btn-primary" ).addClass( "btn-default" );

                document.getElementById('likesNumb' + rowNr).innerHTML = "&nbsp;&nbsp;" + roundLargeNumbers(res[1], 2);
                // console.log("UPDATED LIKES: " + res[1]);
            }
    }});
}






// SET OPTIONS ------------------------------------
function setOptions(fullName, userID, qObjID) {
	// REPORT USER BUTTON
	document.getElementById("reportUserButt").innerHTML = "<a id='reportUserButt' href='javascript:reportUser(\"" + userID + "\");'> <h5>Report @" + fullName + "<span style='opacity: 0'> -- " + userID + "</h5></a>";
	
	// REPORT QUESTION BUTTON	
	document.getElementById("reportQuestionButt").innerHTML = "<a id='reportQuestionButt' href='javascript:reportQuestion(\"" + qObjID + "\");'> <h5>Report Question <span style='opacity: 0'> —- " + qObjID + "</h5></a>";
	
}







// REPORT A USER ---------------------------------------
function reportUser(userID) {
	$('#optionsModal').modal('hide');

	console.log("userID: " + userID);

    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Reporting User, please wait...";
    $('#loadingModal').modal('show');


    $.ajax({
            url:"report-user.php?userID=" + userID, 
            type: "GET", 
            
            success:function(data) {
                var results = data.replace(/\s+/, ""); //remove any trailing white spaces from returned string
                console.debug(results);

                $('#loadingModal').modal('hide');

                alert("Thanks for reporting this user! We will check it out within 24h.");

                // Reload page
                setTimeout(function(){
                    location.reload();
                }, 100);

            // error
            }, error: function (e) { alert(e);}
        });
}






// REPORT A QUESTION ---------------------------------------
function reportQuestion(qObjID) {
	$('#optionsModal').modal('hide');
			
    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Reporting Question, please wait...";
    $('#loadingModal').modal('show');


    $.ajax({
            url:"report-question.php?qObjID=" + qObjID, 
            type: "GET", 
            
            success:function(data) {
                var results = data.replace(/\s+/, ""); //remove any trailing white spaces from returned string
                console.debug(results);

                $('#loadingModal').modal('hide');

                alert("Thanks for reporting this Question! We will check it out within 24h.");

                // Reload page
                setTimeout(function(){
                    location.reload();
                }, 100);

            // error
            }, error: function (e) { alert(e);}
        });
}




// SHOW LOADING MODAL
function showLoadingModal() {
    // Hide Search modal
    $('#searchModal').modal('hide');

    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Please wait...";
    $('#loadingModal').modal('show');
}



function showLoadingModal2() {
    // Hide Add Question modal
    $('#addQuestionModal').modal('hide');

    // Show loading modal
    document.getElementById("loadingText").innerHTML = "Posting your Question, please wait...";
    $('#loadingModal').modal('show');

    // Reload page
    setTimeout(function(){
        location.reload();
    }, 1000);
}





// ROUND LARGE NUMBER JS FUNCTION -----------------------------
function roundLargeNumbers(number, decPlaces) {
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10,decPlaces);

    // Enumerate number abbreviations
    var abbrev = [ "K", "M", "B", "T" ];

    // Go through the array backwards, so we do the largest first
    for (var i=abbrev.length-1; i>=0; i--) {

        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10,(i+1)*3);

        // If the number is bigger or equal do the abbreviation
        if(size <= number) {
             // Here, we multiply by decPlaces, round, and then divide by decPlaces.
             // This gives us nice rounding to a particular decimal place.
             number = Math.round(number*decPlaces/size)/decPlaces;

             // Handle special case where we round up to the next abbreviation
             if((number == 1000) && (i < abbrev.length - 1)) {
                 number = 1;
                 i++;
             }

             // Add the letter for the abbreviation
             number += abbrev[i];

             // We are done... stop
             break;
        }
    }

    return number;
}
</script>
	
</body>
</html>