<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();


/* Variables */
$aObjID = $_GET['aObjID'];
$reportMessage = 'Inappropriate answer';

// Get Answer Object
$aObj = new ParseObject("Answers", $aObjID);
$aObj->fetch();

$aObj->set('isReported', true);
$aObj->set('reportMessage', $reportMessage);
        
try {
    $aObj->save();

// error
} catch ( ParseException $e){ echo $e->getMessage(); }
?>