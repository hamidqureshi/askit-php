<?php
require 'autoload.php';
include 'Configs.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
use Parse\ParseGeoPoint;
session_start();


/* Variables */
$qObjID = $_GET['qObjID'];
	
// Get current User 
$currUser = ParseUser::getCurrentUser();
$currUserID = $currUser->getObjectId();
$cuFullName = $currUser->get('fullName');

// Get Question Object
$qObj = new ParseObject("Questions", $qObjID);
$qObj->fetch();
$qText = $qObj->get('text');

// Get userPointer
$userPointer = $qObj->get("userPointer");
$userPointer->fetch();
$upUserID = $userPointer->getObjectId();

// Get likedBy
$likedBy = $qObj->get("likedBy");

// Unlike Question
if (in_array($currUserID, $likedBy)) {
	try {	
		$likedBy = remove_element($likedBy, $currUserID);
		$qObj->setArray('likedBy', $likedBy);
			
		// Subtract 1 like to the question
		$qObj->increment("likes", -1);
		$qObj->save();

		// echo updated likes
		echo 'LIKE-'.$qObj->get('likes');

	// error
	} catch (ParseException $e){ echo $e->getMessage(); }


// like question
} else {
	try {
		array_push($likedBy, $currUserID);
		$qObj->setArray('likedBy', $likedBy);

		// Add 1 like to the question
		$qObj->increment("likes", 1);
		$qObj->save();
			

		// Send Push Notification
        $pushMessage = $cuFullName.' liked your Question: '.$qText;
        $alert = array("alert" => $pushMessage);

        // Send Push to iOS and Android devices
        ParseCloud::run( "push", array("someKey" => $upUserID, "data" => $alert) );
        ParseCloud::run( "pushAndroid", array("someKey" => $upUserID, "data" => $alert) );
			
		// Save Activity			
		$actObj = new ParseObject("Activity");
		$actObj->set('currentUser', $userPointer);
		$actObj->set('otherUser', $currUser);
		$actObj->set('text', $pushMessage);    
		$actObj->save();

		// echo updated likes
		echo 'LIKE-'.$qObj->get('likes');

	// error
	} catch (ParseException $e){ echo $e->getMessage(); }
}
  


function remove_element($array,$value) {
	foreach (array_keys($array, $value) as $key) {
		unset($array[$key]);
	}  
return $array;
}
?>